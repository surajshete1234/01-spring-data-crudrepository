package com.trancecoder;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.trancecoder.Repository.ContactRepo;
import com.trancecoder.dataobject.contactDetails;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		ConfigurableApplicationContext ctxt = SpringApplication.run(Application.class, args);

		ContactRepo conrepo = ctxt.getBean(ContactRepo.class);
		
		List<contactDetails> clientAddress = conrepo.findDistinctByClientAddress("Pune");
		
		clientAddress.forEach( add -> {
			System.out.println(add);
		});
		
		

		/*
		 * Iterable<contactDetails> allrecords = conrepo.findAll();
		 * 
		 * allrecords.forEach(all -> { if(all.getClientBonusAmt()!=null &&
		 * all.getClientSalaryAmt()!=null) { Double TotalAmt = 0.0; TotalAmt +=
		 * all.getClientBonusAmt(); TotalAmt += all.getClientSalaryAmt();
		 * all.setClientTotalAmt(TotalAmt); conrepo.save(all); }else if
		 * (all.getClientBonusAmt()==null && all.getClientSalaryAmt()==null) {
		 * all.setClientTotalAmt(null); conrepo.save(all); }else if
		 * (all.getClientBonusAmt()!=null && all.getClientSalaryAmt()==null) {
		 * all.setClientTotalAmt(all.getClientBonusAmt()); conrepo.save(all); }else if
		 * (all.getClientBonusAmt()==null && all.getClientSalaryAmt()!=null) {
		 * all.setClientTotalAmt(all.getClientSalaryAmt()); conrepo.save(all); } });
		 * 
		 * allrecords.forEach(showrec -> { System.out.println(showrec); });
		 */

		/*
		 * List<contactDetails> amountforClient = conrepo.getTotalAmountforClient();
		 * 
		 * amountforClient.forEach(amt -> { Double totalAmt = amt.getClientBonusAmt();
		 * totalAmt += amt.getClientSalaryAmt();
		 * 
		 * System.out.println(amt);
		 * 
		 * System.out.println(totalAmt); });
		 */

		/*
		 * contactDetails condtls = new contactDetails(); condtls.setClientNum("C106");
		 * condtls.setClientPhone(7350119856l); condtls.setClientAddress("chennai");
		 * condtls.setClientBonusAmt(6000.00); condtls.setClientSalaryAmt(600000.00);
		 * conrepo.save(condtls);
		 */

	}

}
