package com.trancecoder.Repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.trancecoder.dataobject.contactDetails;

@Repository
public interface ContactRepo extends CrudRepository<contactDetails, Serializable> {
	
	public List<contactDetails> findByClientPhone(Long C_phno);
	
	@Query("from contactDetails where clientBonusAmt is not null And clientSalaryAmt is not null ")
	public List<contactDetails> getTotalAmountforClient();
	
	public List<contactDetails> findDistinctByClientAddress(String address);

}
