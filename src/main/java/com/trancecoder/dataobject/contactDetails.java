package com.trancecoder.dataobject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CONTACT_DTLS")
public class contactDetails {

	@Id
	private String clientNum;

	@Column(name = "clientAddress")
	private String clientAddress;

	@Column(name = "clientPhone")
	private Long clientPhone;

	@Column(name = "clientBonusAmt")
	private Double clientBonusAmt;

	@Column(name = "clientSalaryAmt")
	private Double clientSalaryAmt;

	@Column(name = "clientTotalAmt")
	private Double clientTotalAmt;

	public String getClientNum() {
		return clientNum;
	}

	public void setClientNum(String clientNum) {
		this.clientNum = clientNum;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	public Long getClientPhone() {
		return clientPhone;
	}

	public void setClientPhone(Long clientPhone) {
		this.clientPhone = clientPhone;
	}

	public Double getClientBonusAmt() {
		return clientBonusAmt;
	}

	public void setClientBonusAmt(Double clientBonusAmt) {
		this.clientBonusAmt = clientBonusAmt;
	}

	public Double getClientSalaryAmt() {
		return clientSalaryAmt;
	}

	public void setClientSalaryAmt(Double clientSalaryAmt) {
		this.clientSalaryAmt = clientSalaryAmt;
	}

	public Double getClientTotalAmt() {
		return clientTotalAmt;
	}

	public void setClientTotalAmt(Double clientTotalAmt) {
		this.clientTotalAmt = clientTotalAmt;
	}

	@Override
	public String toString() {
		return "contactDetails [clientNum=" + clientNum + ", clientAddress=" + clientAddress + ", clientPhone="
				+ clientPhone + ", clientBonusAmt=" + clientBonusAmt + ", clientSalaryAmt=" + clientSalaryAmt
				+ ", clientTotalAmt=" + clientTotalAmt + "]";
	}

}
